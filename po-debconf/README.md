# [Questionnaires Debconf](https://www.debian.org/international/french/po-debconf)

[Conseils pour les traducteurs](https://www.debian.org/international/l10n/po-debconf/README-trans.fr.html)

## Pour un relecteur

Un message [RFR] est posté sur la liste avec un `fichier.po` en pièce jointe :

1. Télécharger le `fichier.po` dans ce dossier.
2. Lancer la commande [relire fichier.po](./relire) :
	- Éditer une copie du fichier puis l'enregistrer et fermer l'éditeur.
	- Valider le `fichier.po` avec `msgfmt`.
	- Prévisualiser les écrans debconf avec `podebconf-display-po`.
	- Relire le diff entre l'ancien fichier et la copie modifiée.
3. Répondre au [RFR], avec éventuellement le fichier diff en pièce jointe.
4. Lancer la commande [nettoyer fichier.po](./nettoyer) pour supprimer les fichiers associés à la relecture.

## Pour un traducteur

Un [TAF] ou [MAJ] est envoyé :

1. Envoyer un [ITT].
2. Exécuter la commande [traduire](./traduire) :
	- Éditer le fichier.
	- Valider le `fichier.po` avec `msgfmt`.
	- Prévisualiser les écrans debconf avec `podebconf-display-po`.
	- Relire le diff entre l'ancien fichier  et la copie modifiée.
3. Envoyer un [RFR] avec le fichier en pièce jointe.
4. Pour chaque relecture, lancer `patch `
5. Les relectures sont terminées, lancer la commande [envoyer paquet](./envoyer) :
	- La commande `reportbug` est utilisée pour soumettre un rapport de bug envers le paquet :
		- **Relire** et **éditer** le message prérempli.
		- **Supprimer** les sections non pertinentes.
	- Attendre le retour du BTS pour connaître l'identifiant associé au rapport de bug.
	- Envoyer le [BTS].
6. Lancer la commande [nettoyer](./nettoyer) supprime les fichiers associés à la traduction.
