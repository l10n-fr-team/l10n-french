Please find attached the French debconf templates translation, proofread by the
debian-l10n-french mailing list contributors.

This file should be put as debian/po/fr.po in your package build tree.

Kind regards,
