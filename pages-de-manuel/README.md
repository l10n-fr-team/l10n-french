# [Pages de manuel](https://salsa.debian.org/manpages-l10n-team/manpages-l10n)

[Statistiques](https://manpages-l10n-team.pages.debian.net/manpages-l10n/index.html)

## Statistiques

La commande [statistiques](./statistiques) permet d'afficher l'état actuel des traductions pour chaque section, toutes distributions confondues.

## Travail à faire

La commande [a-faire](./a-faire) liste les pages pour lesquelles vous devez effectuer une mise à jour.
Par défaut, le courriel utilisé est tiré de `git -C man-pages config user.email`, consultez `./a-faire -h` pour modifier cette valeur.

## Préparer des traductions

La commande [preparer](./preparer) prends en arguments une liste de fichiers `.po` à traduire (« malloc.3.po » par exemple) et les copie dans le dossier local.
Il faut bien veiller à ce que le dépôt git soit à jour avant de lancer la commande car elle met à jour les fichiers puis utilise les compendiums pour compléter au maximum leurs traductions avant de les copier dans le dossier local.
La sortie standard liste les arguments et il est donc possible de les stocker dans un fichier :
```
./preparer malloc.3.po mallinfo.3.po > malloc.list
```
et cela facilitera les appels ultérieurs à d'autres commandes par l'utilisation de `xargs` :
```
less malloc.list | xargs ./empaqueter > malloc.tar.xz
```

## Occurrences multiples

Le projet `man-pages` fait un énorme travail pour identifier les chaînes récurrentes dans les différentes pages.
Bien que les scripts `traduire` et `integrer` automatisent cette mise à jour (respectivement depuis et vers) ces fichiers de chaînes récurrentes.

Cependant, il est préférable de traduire **directement** les fichiers contenant les chaînes apparaîssant le plus souvent :
- min-100-occurences.po
- min-020-occurences.po
- min-010-occurences.po
- min-004-occurences.po

Pour cela, la commande [recurrent](./recurrent --extract) génère un extrait du fichier demandé (« min-020-occurences-left.po » pour « ./recurrent 020 »).
Une fois le reste des chaînes traduites, la commande [recurrent](./recurrent --integrate) réincorpore les chaînes traduites dans leur fichier d'origine.

## Traduire

Il faut traduire les fichiers présents dans le dossier local et non ceux présents dans le dépôt.

## Génération des pages

La commande [construire](./construire) est utilisée pour construire les pages localement.
Seules les pages de la distribution « debian-unstable » sont générées pas défaut, consultez `./construire -h` pour modifier ce paramètre.

## Construction d'une archive pour la relecture

La commande [empaqueter](./empaqueter) rassemble les fichiers `.po` source, leurs diffs ainsi que les pages construites avec la commande `construire` dans une archive « .tar.xz ».

## Envoi à la liste pour relecture

Une fois l'archive créée, le message du [RFR] a envoyer peut être généré par la commande [courriel](./courriel) avec les fichiers présents dans l'archive comme arguments.

### Première traduction

L'option « -n » indique qu'il s'agit d'une première traduction.

### Mise à jour

Dans le cas d'une mise à jour, après l'invocation de la commande `courriel`, la commande [statut](./statut) permet d'afficher les commits des dernières modifications faites par le traducteur.
Cela permet notamment d'indiquer la date de dernière modification ainsi que le nom des derniers relecteurs dans votre message à la liste.

## Relecture

Après avoir téléchargé une archive à relire, la commande [extraire](./extraire) développe l'archive dans le dossier local.
Puis la commande [relire](./relire) donne la main, itérativement pour chaque page, sur le fichier `.po` correspondant.

## Incorporation d'une relecture

La commande [incorporer](./incorporer) permet de relire et d'appliquer les diffs envoyés pour les fichiers traduits.
Par la suite, les commandes `construire` et [empaqueter](./empaqueter) peuvent être appelées de nouveau pour pouvoir envoyer à la liste une archive intégrant les relectures.

## Ajout dans le dépôt

Lorsque les fichiers ont été relus, la commande [integrer](./integrer) permet de les ajouter au dépôt après avoir mis à jour les fichiers compendiums.
Si `xargs` est utilisé pour invoquer cette commande, il faut utiliser son option « -o » pour pouvoir interragir avec la commande.

## Nettoyage

La commande [nettoyer](./nettoyer) supprime tous les fichiers associés à une liste de pages.
