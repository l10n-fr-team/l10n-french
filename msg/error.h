extern void xerror(int severity, po_message_t message,
		   char const * filename, size_t line, size_t column,
		   int multiline_p, char const * message_text);

extern void xerror2(int severity,
		    po_message_t message1, char const * filename1, size_t line1, size_t column1,
		    int multiline_p1, char const * message_text1,
		    po_message_t message2, char const * filename2, size_t line2, size_t column2,
		    int multiline_p2, char const * message_text2);
