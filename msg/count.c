#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <gettext-po.h>
#include <unistd.h>

#include "error.h"

int main(int const argc, char const * const * const argv)
{
  if(argc != 1)
    {
      fprintf(stderr, "Usage : %s\n", argv[0]);
      return EXIT_FAILURE;
    }

  struct po_xerror_handler const handler = { .xerror = xerror, .xerror2 = xerror2 };

  po_file_t const file = po_file_read("/dev/stdin", &handler);

  unsigned int count = 0;
  for(char const * const * domain = po_file_domains(file); *domain; domain++)
    {
      char const * const domain_name = *domain;
      po_message_iterator_t iterator = po_message_iterator(file, domain_name);
      bool is_header = true;

      for(po_message_t message = po_next_message(iterator); message != NULL;
	  message = po_next_message(iterator))
	if(is_header)
	  is_header = false;
	else
	  ++count;

      po_message_iterator_free(iterator);
    }

  (void)fprintf(stdout, "%u\n", count);

  po_file_free(file);

  return EXIT_SUCCESS;
}
