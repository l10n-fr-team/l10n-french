# Utilities

## `count`

Lit un fichier `.po`, sur l'entrée standard, et renvoie, sur la sortie standard, le nombre de messages qu'il contient.

## `header`

Affiche l'en-tête du fichier `.po`, lut sur l'entrée standard, dont le nom est passé en argument, ou tous les en-têtes sinon.

## `perlsubpat`

Écrit, sur la sortie standard, un fichier Perl de translittération formé des associations contenues dans les chaînes des fichiers `.po` passés en arguments.

## `perlsub`

Imprime, sur la sortie standard, le résultat de l'application d'un fichier Perl de translittération passé en argument sur toutes les chaînes d'un fichier `.po` lut sur l'entrée standard.

## `unwild`

Inverse **une** (cela devrait être suffisant pour l'utilisation actuelle) extension de paramètres du Shell pour les arguments donnés.

L'exécution de « ./unwild dla-1234.wml dla-1345.wml » donne « dla-1{234,345}.wml ».
Par contre, l'exécution de « ./unwild dla-1234.wml dla-1335.wml » ne donne pas « dla-1{2,3}3{4,5}.wml » mais toujours « dla-1{234,335}.wml ».
