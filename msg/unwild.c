#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int const argc, char const * const * const argv)
{
  if(argc == 1)
    {
      fprintf(stderr, "Usage : %s CHEMIN...\n", argv[0]);
      return EXIT_FAILURE;
    }

  if(argc == 2)
    {
      fprintf(stdout, "%s\n", argv[1]);
      return EXIT_SUCCESS;
    }

  unsigned int size = argc - 1;
  char const * begin[size];
  char const * end[size];
  for(unsigned int i = 0; i < size; ++i)
    {
      begin[i] = argv[1 + i];
      end[i] = begin[i] + (strlen(argv[1 + i]) - 1);
    }

  unsigned int prefix = 0;
  while(begin[0][prefix] != '\0')
    {
      for(unsigned int i = 1; i < size; ++i)
	{
	  if(begin[i][prefix] != begin[0][prefix])
	    goto suffix;
	}

      ++prefix;
    }

  unsigned int suffix = 0;
 suffix:
  while(true)
  {
    for(unsigned int i = 0; i < size; ++i)
      {
	if((end[i] - suffix < begin[i] + prefix)
	   || *(end[i] - suffix) != *(end[0] - suffix))
	  goto done;
      }

    ++suffix;
  }

 done:
  fprintf(stdout, "%.*s{", (int)prefix, begin[0]);

  for(unsigned int i = 0; i < size; ++i)
    {
      begin[i] += prefix;
      end[i] -= suffix;
    }

  for(unsigned int i = 0; i < size; ++i)
    {
      int const length = end[i] - begin[i] + 1;
      fprintf(stdout, "%.*s%s", length, begin[i],
	      i < size - 1 ? "," : "");
    }

  fprintf(stdout, "}%.*s\n", (int)suffix, end[0] + 1);

  return EXIT_SUCCESS;
}
