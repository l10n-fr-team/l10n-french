#include <stdio.h>
#include <gettext-po.h>

void print(int severity,
	   po_message_t message, char const * filename, size_t line,
	   char const * message_text)
{
  if(severity == PO_SEVERITY_FATAL_ERROR)
    (void)fprintf(stderr, "Erreur : ");
  else
    (void)fprintf(stderr, "Avertissement : ");

  if(message != NULL)
    (void)fprintf(stderr, "pour « %s » ", po_message_msgid(message));

  if(filename != NULL)
    (void)fprintf(stderr, "à la ligne %zu dans %s",
		  line, filename);

  if(message_text != NULL)
    (void)fprintf(stderr, " : %s.", message_text);

  (void)fprintf(stderr, "\n");
}

void xerror(int severity, po_message_t message,
	    char const * filename, size_t line, size_t column,
	    int multiline_p, char const * message_text)
{
  (void)column;
  (void)multiline_p;

  print(severity, message, filename, line, message_text);

  if(severity == PO_SEVERITY_FATAL_ERROR)
    exit(EXIT_FAILURE);
}

void xerror2(int severity,
	     po_message_t message1, char const * filename1, size_t line1, size_t column1,
	     int multiline_p1, char const * message_text1,
	     po_message_t message2, char const * filename2, size_t line2, size_t column2,
	     int multiline_p2, char const * message_text2)
{
  (void)column1;
  (void)column2;
  (void)multiline_p1;
  (void)multiline_p2;

  print(severity, message1, filename1, line1, message_text1);
  print(severity, message2, filename2, line2, message_text2);

  if(severity == PO_SEVERITY_FATAL_ERROR)
    exit(EXIT_FAILURE);
}
