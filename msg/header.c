#include <stdio.h>
#include <stdlib.h>
#include <gettext-po.h>

#include "error.h"

int main(int const argc, char const * const * const argv)
{
  if(argc > 2)
    {
      fprintf(stderr, "Usage : %s [CHAMP]\n", argv[0]);
      return EXIT_FAILURE;
    }

  char const * const field = argc == 2 ? argv[1] : NULL;

  struct po_xerror_handler const handler = { .xerror = xerror, .xerror2 = xerror2 };

  po_file_t const file = po_file_read("/dev/stdin", &handler);

  char const * const headers = po_file_domain_header(file, NULL);

  if(headers != NULL)
    {
      if(field == NULL)
	(void)fprintf(stdout, "%s\n", headers);
      else
	{
	  char const * const value = po_header_field(headers, field);

	  if(value != NULL)
	    (void)fprintf(stdout, "%s\n", value);
	}
    }

  po_file_free(file);

  return EXIT_SUCCESS;
}
