#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gettext-po.h>

#include <sys/wait.h>
#include <unistd.h>

#include "error.h"

void translate(po_message_t message, char const * const with)
{
  int output[2];
  if(pipe(output) == -1)
    {
      perror("pipe()");
      exit(EXIT_FAILURE);
    }

  int input[2];
  if(pipe(input) == -1)
    {
      perror("pipe()");
      exit(EXIT_FAILURE);
    }

  ssize_t const written = write(input[1], po_message_msgid(message),
				strlen(po_message_msgid(message)));

  if(written == -1 || (size_t)written != strlen(po_message_msgid(message)))
    {
      perror("write()");
      exit(EXIT_FAILURE);
    }

  (void)close(input[1]);

  pid_t const pid = fork();

  if(pid == -1)
    {
      perror("fork()");
      exit(EXIT_FAILURE);
    }
  else if(pid == 0)
    {
      (void)close(output[0]);

      if(dup2(input[0], fileno(stdin)) == -1)
	{
	  perror("dup2()");
	  exit(EXIT_FAILURE);
	}

      if(dup2(output[1], fileno(stdout)) == -1)
	{
	  perror("dup2()");
	  exit(EXIT_FAILURE);
	}

      char * const exe = "/usr/bin/perl";
      char * const argv[] = { "/usr/bin/perl", (char *)with, NULL };
      extern char ** environ;

      (void)execve(exe, argv, environ);
      perror("execve()");
      exit(EXIT_FAILURE);
    }
  else
    {
      (void)close(input[0]);
      (void)close(output[1]);

      int status;
      if(waitpid(pid, &status, 0) == -1)
	{
	  perror("waitpid()");
	  exit(EXIT_FAILURE);
	}

      if(!WIFEXITED(status) || WEXITSTATUS(status) != 0)
	{
	  perror("Error");
	  exit(EXIT_FAILURE);
	}

      char buffer[8192] = {0};

      if(read(output[0], &buffer, 1024) == -1)
	{
	  perror("read()");
	  exit(EXIT_FAILURE);
	}

      po_message_set_msgstr(message, buffer);
    }
}

void process(po_file_t file, char const * templates)
{
  for(char const * const * domain = po_file_domains(file); *domain; domain++)
    {
      char const * const domain_name = *domain;
      po_message_iterator_t iterator = po_message_iterator(file, domain_name);
      bool is_header = true;

      for(po_message_t message = po_next_message(iterator); message != NULL;
	  message = po_next_message(iterator))
	{
	  if(is_header)
	    is_header = false;
	  else
	    translate(message, templates);
	}

      po_message_iterator_free(iterator);
    }
}

int main(int const argc, char const * const * const argv)
{
  if(argc != 2)
    {
      (void)fprintf(stderr, "Usage : %s TEMPLATE.pl\n", argv[0]);
      return EXIT_FAILURE;
    }

  char const * const template = argv[1];

  struct po_xerror_handler const handler = { .xerror = xerror, .xerror2 = xerror2 };

  po_file_t const file = po_file_read("/dev/stdin", &handler);

  process(file, template);

  (void)po_file_write(file, "/dev/stdout", &handler);

  po_file_free(file);

  return EXIT_SUCCESS;
}
