# Équipe de traduction en français de Debian

Avant toute chose, lire les [pages de l'équipe](https://www.debian.org/international/french/).

Liens utiles :
* [Liste de diffusion](https://lists.debian.org/debian-l10n-french/)
* [Canal IRC](irc:/irc.debian.org/#debian-l10n-fr)
* [État actuel du travail en cours](https://l10n.debian.org/coordination/french/fr.by_translator.html)

Ce projet concentre un ensemble de commandes pour aider à la gestion, traduction et relecture des différents types de documents à traduire comme :

* les [pages du site internet](site-internet/README.md) ([www.debian.org](https://www.debian.org)) ;
* les [questionnaires Debconf](po-debconf/README.md) ;
* les [pages de manuel](pages-de-manuel/README.md).

Toutes les commandes sont documentées par leurs options « -h » et « --help ».

Ne pas hésiter à proposer des solutions pratiques et améliorations au reste de l'équipe, de façon convergente si possible, pour que tout le monde, y compris les nouveaux contributeurs, puisse en profiter ; et que la mémoire des processus de traduction des différents documents de Debian soit convervée collectivement.

La [foire aux questions](./FAQ.md) peut répondre à certaines interrogations.

## Installation

L'exécution du script [installer](./installer) est nécessaire au bon fonctionnement des différents scripts.

Dans un premier temps, différents dépôts [Salsa](https://salsa.debian.org/) sont optionnellement clonés.
Il n'est pas nécessaire de cloner les dépôts pour faire de la relecture uniquement. 

Dans un second temps, des fichiers de configuration par défaut sont installés dans le dossier `config` :
+ [pseudo](./config/.pseudo) : pseudo de l'utilisateur utilisé pour nommer les fichiers `.diff` temporaires à envoyer à la liste ;
+ [fullname](./config/.fullname) : nom complet de l'utilisateur utilisé pour remplir le champ `maintainer` des pages du site ;
+ [diff.editor](./config/.diff.editor) : éditeur de fichiers `.diff`, le premier argument étant le fichier original et le second le fichier traduit ;
+ [po.editor](./config/.po.editor) : éditeur de fichiers `.po` ;
+ [po.as-text.editor](./config/.po.as-text.editor) : éditeur de texte (sans mode spécifique afin d'impacter le moins possible le fichier `.po` lors d'une relecture)  ;
+ [wml.editor](./config/.wml.editor) : éditeur de fichier `.wml` ;
+ [html.viewer](./config/.html.viewer) : pour un aperçu des pages du site internet.

Il est possible de modifier les fichiers manuellement si la configuration par défaut affichée lors de l'installation n'est pas satisfaisante.

### Dépendances

Liste des paquets Debian à installer :
```
git tidy wml gettext coreutils make gcc g++ libgettextpo-dev bc xclip
```

## Notes

Sauf cas contraire, les scripts **n'effectuent pas** d'actions sur les dépôts Git, tout est à faire manuellement.
Si un script effectue une action sur un dépôt, un message de confirmation sera **toujours** affiché.
Dans tous les cas, **aucun push** ne sera fait automatiquement.

### Annuaire

La commande [annuaire](./annuaire) permet de gérer les noms et courriels des relecteurs afin de faciliter l'écriture des messages de commit.
