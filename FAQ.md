# Foire aux questions

## Comment utiliser d'autres éditeurs ou visionneuses que ceux proposés par défaut ?

En éditant les fichiers de configuration présents dans le dossier `config` et ne commençant pas par un « . ».

Par exemple, pour que `vim` soit utilisé pour éditer les fichiers `.wml`, il faut remplacer le contenu de `config/wml.editor` par :
```
vim $@
```

Il est possible de passer des options spécifiques, comme « -O » dans le cas ci-dessus pour ouvrir l'original et la traduction dans la même fenêtre :
```
vim -O $@
```

## Faut-il télécharger les dépôts pour contribuer ?

Pour relire non, pour traduire oui.

## Les dépôts sont déjà présents sur la machine, comment éviter de télécharger à nouveau chaque dépôt ?

En copiant, avant de lancer `installer`, les dossiers contenant les dépôts :

- [du site internet](https://salsa.debian.org/webmaster-team/webwml) vers le dossier `site-internet/webwml` ;
- [des pages de manuel](https://salsa.debian.org/manpages-l10n-team/manpages-l10n) vers le dossier `pages-de-manuel/man-pages`.

Seul l'ajout de serveurs distants sera alors demandé lors de l'exécution de la commande `installer`.

## Comment ajouter une translittération automatique pour les pages du site internet

Il faut tout d'abord trouver à quel fichier `.po` du dossier [po/](./site-internet/po/) se rapporte la chaîne.
Si aucun ne convient, il faut en créer un nouveau qui sera automatiquement pris en compte par le script `translitterer.pl`.

Une fois la paire de « msgid » et « msgstr » correspondant à la nouvelle translittération ajoutée au fichier, la commande [po/trier](./site-internet/po/trier) doit être appelée.
Enfin, si une vérification est souhaitée, il est possible d'éditer un fichier dans le dossier [t/](./site-internet/t) pour y ajouter la chaîne en anglais, puis de lancer la commande `./translitterer t/fichier.wml` et de consulter le résultat affiché sur la sortie standard pour confirmer que la translittération a bien été faite.
