# Site internet [www.debian.org](https://www.debian.org)

[Statistiques](https://www.debian.org/devel/website/stats/fr)

## Traduction d'une page

La commande [traduire](./traduire) initie ou met à jour des traductions et pour chacune d'entre elle :

1. initialise le fichier ou met à jour les informations « translation » et « maintainer » de la page ;
2. applique les [translittérations pour les annonces de sécurité](Translittérations pour les annonces de sécurité) :
	- les fichiers `.po` dans le dossier [po/](./po/) sont utilisés pour générer un fichier `transducteur.pl`
	- l'ordre dans lequel les fichiers sont lus n'est pas important
	- l'ordre dans lequel les chaînes sont aggrégées n'est pas important
	- les translittérations sont appliquées de façon décroissante suivant leur longueur
3. donne la main pour **éditer le fichier** ;
4. vérifie la validité de la page une fois construite (une erreur n'arrête pas la procédure, voir la sortie d'erreur pour **déterminer quelle action à effectuer**) ;
5. ouvre le fichier html généré pour **prévisualiser le résultat** ;
6. donne la main pour **éditer le diff généré**.

### Translittérations pour les annonces de sécurité

Certaines parties des pages étant récurrentes, les fichiers `.po` du dossier `po/` sont utilisés pour générer un `transducteur.pl` pour effectuer une translittération.
Pour cela, ces chaînes doivent être lisibles par `perl` sous le format classique de [*PCRE*](https://perldoc.perl.org/perlre), « s|<msgid>|<msgstr>|g ».
Par exemple,

```
msgid "For the detailed security status of (.+?) please refer to its security tracker page at: "
msgstr "Pour disposer d'un état détaillé sur la sécurité de $1, veuillez consulter sa page de suivi de sécurité à l'adresse : "
```
permet de traduire cette partie de message tout en y remplaçant le nom du paquet.
Il est à noter que contrairement à d'habitude, les msgids ne contiennent pas de « % » pour indiquer un paramètre, mais sont utilisés à la place un motif de remplacement dans le msgid (« (.+?) » dans l'exemple ci-dessus) et un motif de substitution dans le msgstr (« $1 » dans l'exemple ci-dessus).

Il est également possible d'utiliser des chaînes traditionnelles telles que :
```
msgid "remote server"
msgstr "serveur distant"
```
auquel cas, toute correspondance exacte sera remplacée.

Dans certains cas, le passage d'un pluriel de l'anglais au français à le bon goût de préserver les « s » auquel cas
```
msgid "remote server(s?)"
msgstr "serveur$1 distant$1"
```
permet de traiter le singulier et le pluriel avec une seule expression.
Ce n'est toutefois pas toujours le cas comme avec « special parameter(s?) » par exemple.

Il est également possible de spécifier, via un commentaire, quel doit être le [format](https://perldoc.perl.org/perlre#Modifiers) utilisé pour une chaîne particulière, par un ensemble de lettres, suivies d'un symbole puis d'un autre ensemble de lettres comme dans :
```
# s|gi
msgid "security update"
msgstr "Mise à jour de sécurité"
```
qui sera transformé en « s|security update|Mise à jour de sécurité|gi ».

Les fichiers utilisés pour la translittération sont :
+ [security.po](po/security.po) pour le cadre général des DLA et DSA ;
+ [english.po](po/english.po) pour des mots ou bouts de phrases ;
+ [name.po](po/name.po) pour des noms propres ;
+ [lexicon.po](po/lexicon.po) pour des termes techniques ;
+ [vulnerability.po](po/vulnerability.po)  pour les vulnérabilités ;
+ [issue.po](po/issue.po) pour les problèmes ;
+ [attack.po](po/attack.po) pour les attaques.

Tout fichier `.po` ajouté dans le dossier `po/` sera utilisé pour la translittération.

Le script [po/trier](./po/trier) peut être appelé dans ce dossier afin d'ordonner alphabétiquement certains fichiers.
Il est **préférable** d'appeler cette commande avant l'ajout des modifications au dépôt, le tri alphabétique des chaînes rendant plus simple leur maintenance.

Le dossier `./t` contient des tests que les traducteurs peuvent mettre en place afin de vérifier qu'une translittération se passe correctement.
L'appel `./translitterer t/test.wml` en est une illustration.

## Envoi à la liste pour relecture

Le script [integrer](./integrer) permet d'alléger la routine de partage du fichier dans le dépôt et d'envoi du courriel à la liste.

La première fois que le document est traduit ou relu par le traducteur en charge, l'option « --first » **doit** être passée à la commande.
Elle ajoute alors les fichiers indiqués dans le dépôt puis effectue un commit avec un message prédéfini qui peut être modifié.
Enfin, le message de premier [RFR] est copié dans le presse-papier pour faciliter l'envoi de ce courriel.

## Relecture

1. Exécuter la commande [relire](./relire), pour ouvrir une à une les copies des fichiers ainsi que le fichier original correspondant, de deux façons possibles :
	- télécharger les fichiers à relire depuis le message de [RFR] (« ./relire dla-2600.wml dla-2601.wml » par exemple) ;
	- mettre à jour le dépôt Git et copier l'intitulé du [RFR] (« ./relire lts/security/2021/dla-260{0,1}.wml » par exemple) ;
2. envoyer votre travail à la liste (les fichiers diff restants) ;
3. exécuter la commande [nettoyer](./nettoyer), avec les mêmes arguments que ceux donnés à `relire`, pour supprimer tous les fichiers créés.

## Intégration d'une relecture

Cette étape est à faire manuellement pour le moment, cependant, la commande [integrer](./integrer) peut à nouveau être utilisée, sans l'option « --first », afin de créer un commit au nom du relecteur.

## Nettoyage

La commande [nettoyer](./nettoyer) supprime tous les fichiers locaux liés à une liste de pages.

## Maintenance

### [verification_po_et_pot](./verification_po_et_pot)

Ce script permet de vérifier l'état des fichiers **PO** et **POT** du dépôt du site web de Debian.

#### Fonctionnement

Il effectue les actions suivantes dans l'orde :
+ Mise à jour des fichiers **POT** dans le répertoire `english/po/`,
+ Pour chaque fichier **POT**, vérification de l'existance du fichier **PO** correspondant pour chaque langue,
+ Met à jour les fichiers **PO** à partir des fichiers **POT**,
+ Retourne l'état actuel du dépôt Git.

#### Variables

Les variables suivantes peuvent êtres transmises :
+ **REPO** : Permet de surcharger l'emplacement du répertoire de travail. Par défaut `./webwml`.
+ **DEBUG** : Permet d'effectuer une sortie verbeuse. Par défaut `false`.

#### Exemples d'utilisation

Exécution standard :
```
./verification_po_et_pot
```

Surcharge de l'emplacement du répertoire de travail :
```
REPO="~/salsa/webwml" ./verification_po_et_pot
```

Exécution en mode débug :
```
DEBUG=true ./verification_po_et_pot
```
