#!/bin/bash

################################################################################

# Copyright (C) 2020, Alban Vidal <zordhak@debian.org>

# SPDX-License-Identifier: GPL-3.0-or-later
# License-Filename:        LICENSE_GPL-3.0

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################################

# Repository URL of this script:
# https://salsa.debian.org/l10n-fr-team/scripts

################################################################################

REPO=${REPO:="./webwml"}

DEBUG=${DEBUG:=false}
DEBUG_SLEEP_TIME="0.5"

################################################################################

if [ ! -d $REPO ] ; then
    echo "ERROR - $REPO is not a directory"
    exit 1
fi

echo -e "\n################################################################################\n"

# Create languages list array
declare -a LANG_LIST
LANG_LIST_INDEX=0

# Search availaibles languages
for LANG_DIR in $(find $REPO -mindepth 1 -maxdepth 1 -type d) ; do

    $DEBUG && echo -e "\nDEBUG: LANG_DIR=$LANG_DIR"

    # Extract just dir name
    DIR_NAME=$( basename $LANG_DIR | sed 's/\..*//')
    $DEBUG && echo -e "DEBUG: DIR_NAME=$DIR_NAME"

    # Clean unwanted directories
    case $DIR_NAME in
	ci|english|Perl|locale|"" )
	    # Note: "" is used to exclude all hidden directories
	    # skip
	    $DEBUG && echo -e "DEBUG: $DIR_NAME => SKIP"
	    ;;
	* )
	    # Add current language in array
	    $DEBUG && echo -e "DEBUG: $DIR_NAME => Adding to array LANG_LIST"
	    LANG_LIST[$LANG_LIST_INDEX]=$DIR_NAME
	    (( LANG_LIST_INDEX ++ ))
	    ;;
    esac

    $DEBUG && echo -e "DEBUG: $DIR_NAME - End of loop - sleep $DEBUG_SLEEP_TIME"
    $DEBUG && sleep $DEBUG_SLEEP_TIME

done

echo -e "${#LANG_LIST[@]} langs detected"

$DEBUG && echo -e "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$DEBUG && echo -e "DEBUG: End of loop LANG_DIR"
$DEBUG && echo -e "DEBUG: Number of languages=${#LANG_LIST[@]}"
$DEBUG && echo -e "DEBUG: sleep $DEBUG_SLEEP_TIME"
$DEBUG && echo -e "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$DEBUG && sleep $DEBUG_SLEEP_TIME

echo -e "\n################################################################################\n"

echo -e "Search if all WML translated files exists in english\n"
echo -ne "..."

WML_OK=true

for CURRENT_LANG in ${LANG_LIST[@]} ; do

    $DEBUG && echo -e "DEBUG: CURRENT_LANG=$CURRENT_LANG"
    $DEBUG && sleep $DEBUG_SLEEP_TIME

    # search all WML files
    # note: exclude international directory
    for WML in $(find $REPO/$CURRENT_LANG -type f -not -path "$REPO/$CURRENT_LANG/international/*" -name *.wml) ; do
	#$DEBUG && echo -e "DEBUG: "
	WML_FILENAME=$( echo $WML|sed "s#$REPO/$CURRENT_LANG##")
	$DEBUG && echo -e "DEBUG: WML_FILENAME=$WML_FILENAME"
	$DEBUG && sleep $DEBUG_SLEEP_TIME
	# test if file exist in english
	if [ ! -f "$REPO/english${WML_FILENAME}" ] ; then
	    echo -e "\rWARN - $WML"
	    echo -ne "..."
	    WML_OK=false
	fi
    done
done

if $WML_OK ; then
    echo -e "\rGreat, all WML translated files exists in english"
else
    echo -e "\r   "
    echo "Warning, at least one WML translated file has not original english WML file"
fi

echo -e "\n################################################################################\n"
