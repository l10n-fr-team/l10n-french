#!/bin/sh -e

usage () {
    echo "$0 [-h,--help] [webwml/(english|french)/]PAGE.wml ...

    Initie la traduction des PAGEs.
    La commande est réentrante.

    Le hash de traduction est soit initialisé soit mis à jour depuis la dernière modification du source.

    Les fichiers DSA et DLA sont translittérés s'ils ne sont pas déjà traduits (consultez la commande ./translitterer)."
}

eval set -- $(getopt --longoptions "help" --options "h" -- "$@")

while true
do
    case $1 in
	-h|--help)
	    usage
	    exit 0
	    ;;
	*)
	    shift
	    break
	    ;;
    esac
    shift
done

for file in $@
do
    case "$file" in
	webwml/english/*.wml)
	    source="$file"
	    target=$(echo "$file" | sed 's#webwml/english#webwml/french#g')
	    ;;
	webwml/french/*.wml)
	    source=$(echo "$file" | sed 's#webwml/french#webwml/english#g')
	    target="$file"
	    ;;
	*.wml)
	    source="webwml/english/$file"
	    target="webwml/french/$file"
	    ;;
	*)
	    echo "Pas de fichier valable en argument."
	    exit 1
	    ;;
    esac

    FULLNAME="$(../config/fullname)"

    if test -f "$target"
    then
	# Si le fichier existe déjà, mise-à-jour du hash de traduction.
	filepath="$(echo $source | cut -d / -f 1 --complement)"
	hash=$(git -C webwml log -1 "$filepath" | head --lines 1 | sed 's#commit ##')
	sed --in-place=.bak "s#translation=\"\([0-9a-f]*\)\"#translation=\"$hash\"#g" "$target"
	sed --in-place=.bak "s#maintainer=\"\(.*\)\"#maintainer=\"$FULLNAME\"#g" "$target"
	rm "$target.bak"
	existed=true
    else
	# Si le fichier n'existe pas déjà, utilisation de la commande copypage.pl de webwml.
	cd webwml
	./copypage.pl -l "french" -m "$FULLNAME" "$(echo $source | cut -d / --complement -f 1)"
	cd ..
	existed=false
    fi

    filename="$(basename $target)"

    # Première traduction automatique des DLA et DSA.
    if test $existed = false
    then
	case "$filename"
	in
	    dsa-*.wml | dla-*.wml)
		out=$(mktemp --suffix .wml)
		./translitterer "$target" > "$out"
		mv "$out" "$target"
		;;
	esac
    fi

    while true
    do
	# Edition manuelle.
	../config/wml.editor "$source" "$target"

	# Construction de la page finale, nettoyage des erreurs et affichage.
	make -C $(dirname "$target") $(echo $filename | sed 's#.wml$#.fr.html#g')
	tidy -errors -quiet -utf8 "$(echo $target | sed 's#.wml$#.fr.html#g')" || true
	../config/html.viewer "$(echo $target | sed 's#.wml$#.fr.html#g')"

	again=""
	read -p "Souhaitez-vous de nouveau éditer la page $target (o/N) ? " again

	if test "$again" != "o"
	then
	    break
	fi
    done

    # Suppression du fichier construit (optionnel ?).
    rm "$(echo $target | sed 's#.wml$#.fr.html#g')"
    cp "$target" "$filename"

    if test $existed = true
    then
	PSEUDO="$(../config/pseudo)"

	git -C webwml diff "$(echo $target | cut -d / -f 1 --complement)" > "$filename-$PSEUDO.diff"

	if test "$(wc --lines $filename-$PSEUDO.diff | cut -d ' ' -f 1)" -ne 0
	then
	    ../config/diff.editor "$filename-$PSEUDO.diff"
	else
	    rm "$filename-$PSEUDO.diff"
	fi
    fi
done
